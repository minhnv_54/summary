>ref: https://www.smashingmagazine.com/2018/01/understanding-using-rest-api/  
https://realpython.com/api-integration-in-python/  
https://medium.com/@wkrzywiec/rest-restful-web-service-api-soap-whats-the-difference-4f101953d0bd  
https://rapidapi.com/blog/api-vs-web-service/ 
### Call API bằng Postman 
>Source API :http://dummy.restapiexample.com/  

**GET** - lấy tài nguyên từ máy chủ   
![Screenshot_from_2020-04-27_22-21-09](/uploads/7151b02a7ecb4c6147c539ff878de03b/Screenshot_from_2020-04-27_22-21-09.png)  
**POST** - Tạo tài nguyên mới trên máy chủ 
![Screenshot_from_2020-04-27_22-28-30](/uploads/ab9cffbef1f0b16462c703a3f90ee8e7/Screenshot_from_2020-04-27_22-28-30.png) 
**PUT** - Cập nhật tài nguyên trên máy chủ 
![Screenshot_from_2020-04-27_22-32-04](/uploads/d0eb9b4032da7056dc25ddcc5f0448bb/Screenshot_from_2020-04-27_22-32-04.png)  
**DELETE** - Xóa tài nguyên trên máy chủ 
![Screenshot_from_2020-04-27_22-44-19](/uploads/fe7bb84f2df587e4c467cbb1433e610b/Screenshot_from_2020-04-27_22-44-19.png)