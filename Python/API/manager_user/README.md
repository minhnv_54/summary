app.py:
- Sử dụng database (sqlite3) để tạo bảng chứa thông tin về user
- Sử dụng đường dẫn động để kết nối đến database 
- Thông tin về password của user được mã hóa bằng hash (blake2b)
- Xác thực trước khi call API (sử dụng jwt tạo token)
- Viết API cho các route, dùng `delete soft` khi xóa user
- Phân quyền: dựa vào trường role của user để ckeck quyền call API