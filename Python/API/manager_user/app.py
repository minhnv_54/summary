import flask
import sys
from flask import request, jsonify
import argparse
from flask_jwt_extended import JWTManager, jwt_refresh_token_required, create_refresh_token, get_jwt_identity, jwt_required, create_access_token, verify_jwt_in_request, get_jwt_claims
import datetime
from functools import wraps
import sqlite3
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
import sqlalchemy.exc
from hashlib import blake2b

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config['JWT_SECRET_KEY'] = 'super-secret'  # Change this!
app.config['JSON_SORT_KEYS'] = False
jwt = JWTManager(app)

# Truyền đường dẫn database từ dòng lệnh , ex: python user.py "data/user.db"
parser = argparse.ArgumentParser(description='path file')
parser.add_argument('path', help='path file', nargs='?', default='/home/tmp/api.db')
data = parser.parse_args()
path_db = data.path

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + str(path_db)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
#hash password by blake2b
def hash_pass(password):
    return blake2b(password.encode('UTF-8')).hexdigest()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(30), unique=True, nullable=False)
    password = db.Column(db.String, nullable=False)
    age = db.Column(db.Integer)
    role = db.Column(db.String(20), server_default="viewer")
    delete_at = db.Column(db.DateTime(timezone=True))
    deleted = db.Column(db.Boolean(), default=False)
# kiem tra duong dan file va tao table
try:
    db.create_all()
except :
    print("Duong dan file khong ton tai")
    sys.exit(0)

# Check user/pass trước khi call API
@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400
    ckeck_user = User.query.filter_by(user=username, deleted=0)

    for check in ckeck_user:
        if hash_pass(password) == check.password:
            identity = {
                'user': username,
                'role': check.role
            }
        access_refresh = {
                'access_token': create_access_token(identity=identity),
                'refresh_token': create_refresh_token(identity=identity)
               }
        return jsonify(access_refresh), 200
    return jsonify({"msg": "Invalid username or password"}), 401

#Lấy access_token khi hết hạn (default: 300s-access_token va 30days- refresh_token
@app.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    access_token = {
        'access_token': create_access_token(identity=current_user)
    }
    return jsonify(access_token), 200

# Check role của user (access nếu role == admin)
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        current_user = get_jwt_identity()
        if current_user['role'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper

#List all users dưới dạng json (required role = admin)
@app.route('/users/trash', methods=['GET'])
@jwt_required
def get_trash():
    users = []
    get_user = User.query.filter_by(deleted=1).all()
    for row in get_user:
        user_ne = {"id": row.id, "user": row.user, "password": row.password, "age": row.age, "role": row.role, "delete_at": row.delete_at }
        users.append(user_ne)
    if len(users) == 0:
        return jsonify("Data empty"), 200
    return jsonify(users), 200

@app.route('/users', methods=['GET'])
@admin_required
def get_users():
    users = []
    get_user = User.query.filter_by(deleted=0).all()
    for row in get_user:
        a = {"id": row.id, "user": row.user, "password": row.password, "age": row.age, "role": row.role }
        users.append(a)
    if len(users) == 0:
        return jsonify("Data empty"), 404
    return jsonify(users), 200

# Get one user by id from the path
@app.route("/user/<id>", methods=['GET'])
@jwt_required
def get_user_by_id_in_path(id):
    user = users.query.get(id)
    if user is None:
        return 'ID is not exists', 404
    data = {
        "id": user.id,
        "user": user.user,
        "age": user.age,
        "role": user.role
    }
    return jsonify(data)



# Add new user
@app.route('/user', methods=['POST'])
@jwt_required
def post_users():
    try:
        new_user = {"user": request.json["user"], "password": request.json["password"], "age": request.json["age"]}
    except :
        return 'New data must be in the right format: {"user": "new_username", "password": "new_password", "age": new_age}")', 400
    try:
        pass_hash = hash_pass(request.json["password"])
        new_users = User(user=new_user["user"], password=pass_hash, age=new_user["age"])
        db.session.add(new_users)
        db.session.commit()
    except sqlalchemy.exc.IntegrityError:
        return jsonify("Username already exists. Please try another one"), 400

    return jsonify({"new user": new_user}), 201


# Update user
@app.route('/user', methods=['PUT'])
@jwt_required
def put_users():
    try:
        username = flask.request.json['user']
        new_age = flask.request.json['age']
        new_password = flask.request.json['password']
    except:
        return 'New data must be in the right format: {"user": "your_username", "password": "new_password", "age": new_age}', 400
    if (type(new_age) != int) or (new_age < 18):
        return 'Age must be an INTEGER and 18+', 400
    if ((type(username) or type(new_password)) != str) or ((len(username) or len(new_password)) > 12):
        return 'Username/password must be a STRING and could not have more than 12 characters', 400

    user = users.query.filter_by(user=username)
    if user is None:
        return 'User is not exists', 404
    for i in user:
        i.password = new_password
        i.age = new_age
    db.session.commit()
    return "Update user successfully", 201


# Delete user by id
@app.route('/user/<id>', methods=['DELETE'])
# @jwt_required
def delete_user(id):
    if flag_errol:
        return "Unable open database", 500
    user = User.query.get(id)

    if user == None:
        return "ID not match"
    if user.deleted:
        return "User deleted"
    user.deleted = True
    user.delete_at = func.now()
    db.session.commit()
    return jsonify({'deleted_user': user.user}), 200


app.run(debug=True, host='0.0.0.0')
# curl -H "Content-Type: application/json"  -XPOST -d '{"name":"Adam","age":14,"id":1}' http://localhost:5000/user
# curl -H "Content-Type: application/json"  -XPUT -d '[{"name":"Eva","age":14,"id":1}]' http://localhost:5000/user
# curl -X DELETE http://localhost:5000/user/1
