import requests
from bs4 import BeautifulSoup
import pandas as pd
import argparse

# pip3 install -r requirement.txt
# python3 collect_to_excel.py 'https://gadgets.dantri.com.vn/corona' '/home/intern/minh.xlsx'

def solution(url, output):
    try:
        req = requests.get(url)
    except requests.exceptions.MissingSchema:
        print("Cú pháp sai: yêu cầu : $python file.py 'url' 'path/filename.xlsx'")
        return 0
    except requests.exceptions.ConnectionError:
        print(" Sai đường dẫn URL: This site can’t be reached ")
        return 0

    soup = BeautifulSoup(req.text, "lxml")
    # create array index include index of row
    new_feeds = soup.findAll('tr', class_='ant-table-row ant-table-row-level-0')
    index= [ a.get('data-row-key') for a in new_feeds]
    # get content of row
    country,SCN,TV,SCN1, BP,TV1,BP1 =[],[],[],[],[],[],[]
    TV2,SCN2,BP2=[],[],[]
    for i in index:
        tmp=soup.find('tr', attrs={'data-row-key': i}).td
        country.append(tmp.text.strip())
        a=list(map(str, tmp.next_sibling.text.strip().replace(" ", "").split("\n")))
        b=list(map(str, tmp.next_sibling.next_sibling.text.strip().replace(" ", "").split("\n")))
        c= list(map(str, tmp.next_sibling.next_sibling.next_sibling.text.strip().replace(" ", "").split("\n")))
        a.append('0'),b.append('0'),c.append('0')
        SCN1.append(a[0]),TV1.append(b[0]),BP1.append(c[0])         # so luong hien tai
        SCN2.append(a[1]),TV2.append(b[1]),BP2.append(c[1])         # so luong tang/giam

    # convert to excel
    df = pd.DataFrame()
    df['Country'], df['Số ca nhiễm'], df['Số người tử vong'], df['Số người bình phục']= country, SCN1, TV1, BP1
    df['Số ca nhiễm tăng'], df['Số người tử vong tăng'], df['Số người bình phục tăng ']=SCN2,TV2,BP2
    print(df)
    try:
        df.to_excel(output,index=False)
    except ValueError:
        print("Đường dẫn file không hợp lê, nên để output dạng: path/tenfile.xlsx")
    except PermissionError:
        print("không có quyền lưu file ở đây")
    except FileNotFoundError:
        print("Đường dẫn để lưu không tồn tại")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='test')
    parser.add_argument('url', help='url web',nargs='?')         # thêm option: default='https://gadgets.dantri.com.vn/corona' nếu không truyền var
    parser.add_argument('output', help='path output',nargs='?')  # thêm option:  default='/home/genis/genis.xlsx'
    args = parser.parse_args()
    solution(format(args.url),format(args.output))
