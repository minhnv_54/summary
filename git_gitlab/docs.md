**I.** **Chung**

**1.1. Git là gì?**

>Git là hệ thống quản lý phiên bản phân tán

**1.2. Git làm được gì?**

- Quản lý code và version: quản lý các phiên bản chỉnh sửa =>> giúp lưu lại các phiên bản của những lần thay đổi vào mã nguồn và có thể dễ dàng khôi phục lại dễ dàng mà không cần copy lại mã nguồn rồi cất vào đâu đó.

- Hỗ trợ teamwork: thay cho ghép code thủ công của từng người trong nhóm theo cách truyền thống
( cung cấp khả năng tách nhánh-branch, nên những việc như phân chia task, tổng hợp task đơn giản hơn)

**1.3. Các khái niệm cơ bản**

_-Repository:_

+ Là nơi lưu trữ, quản lý tất cả những thông tin cần thiết cũng như các sửa đổi và lịch sử của toàn bộ dự án

+ Có hai loại repository, đó là local repository và remote repository.

_-Object store và index:_

+ Là 2 cấu trúc dữ liệu chính của Repo

+ Object store: là trái tim của Git, nó chứa dữ liệu nguyên gốc (original data files), các file log ghi chép quá trình sửa đổi,

+ Index: Index không hứa nội dung file mà chỉ dùng để truy tìm (track) những thứ mà bạn muốn commit.

_-Branch:_

+Khi muốn thực hiện các task song song mà không ảnh hưởng đến nhau trong cùng 1 repo thì chúng ta sẽ tạo ra các branch tương ứng, và cuối cùng dùng branch master để merge lại thôi.

_-Git work-flow:_
+Git Flow do Vincent Driessen đề xuất ra.
+Git-Flow gồm có 2 nhánh chính là Master và Develop, 3 nhánh Phụ gồm: Feature Release (Tách từ: master Merge vào: develop và master ), HotFix.(Khi có một bug nghiêm trọng trên bản production cần được giải quyết ngay lập tức, một hotfix branch sẽ được tách ra từ master và được đánh version để nhận biết.)

![enter image description here](https://i.imgur.com/ZlAXOoC.png)
  

.Master branch: thưởng chỉ để merge

.Develop branch: là branch trung tâm cho việc phát triển

**.** Feature: Tách từ: develop Merge vào: develop

**1.4. Các trạng thái có thể có trong git repository**

![enter image description here](https://i.imgur.com/vhaak1b.png)

**+** File ở trạng thái staged mới được commit

**+** Khi tạo 1 file mới thì nó ở trạng thái Untracked

+ Add file đó vào stagging area thì nó sẽ lên luôn trạng thái staged

+Nếu chỉnh sửa file đó thì nó sẽ về trạng thái Modify (dùng lệnh add để lên staged)

+rm ten_file để đưa file từ tracked về untracked mà ko bị xóa file khỏi ổ cứng ( trừ khi dùng git rm -f ten_file)

**II.** **Config cho git với git config**

**2.1 Config thông tin cá nhân**

  ![enter image description here](https://i.imgur.com/hdD1304.png)

confirm:

  ![enter image description here](https://i.imgur.com/egARStE.png)

**2.2 Config default/prefered editor**

 ![enter image description here](https://i.imgur.com/7LR6r2r.png)

  
**2.3 Config global .gitignore**

**-** .gitignore liệt kê những file không muốn cho vào git

![enter image description here](https://i.imgur.com/pU1a341.png)
  

**III. Các thao tác ban đầu**

- Khởi tạo : `git init name_repo`
- Clone:

![enter image description here](https://i.imgur.com/lAG8bu7.png)
  

**IV. Git commit**

example:

![enter image description here](https://i.imgur.com/svBX0tC.png)

-amending: bạn commit nhưng bị quên add một số file nào đó và bạn không muốn tạo ra một commit mới thì có thể sử dụng lệnh commit kết hợp tham số --amend để gộp các  file đó và bổ sung vào commit cuối cùng, vì vậy không tạo ra commit mới.

![enter image description here](https://i.imgur.com/PzIvdoW.png)

**V. Làm việc với branch**

- Create and switch branch:

![enter image description here](https://i.imgur.com/kLMDwP1.png)

- rename branch:

. local branch hiện tại:

![enter image description here](https://i.imgur.com/k2h8B7S.png)

. local branch khác:

![enter image description here](https://i.imgur.com/ulsA1i9.png)

. Delete the old-name remote branch and push the new-name local branch.

`$git push origin :old-name new-name`

.Reset the upstream branch for the new-name local branch.

`$git push origin -u new-name`

**VI. Các thao tác liên quan khác:**

**6.1 Check out đến commit ID**

**-Reset:**

ex: `A - B - C (HEAD -> master)`

`.git reset --soft B`: Sẽ đưa HEAD về vị trí commit B, giữ nguyên các thay đổi đã được git add tại B trong Staging Area

`.git reset B`: Sẽ đưa HEAD về vị trí commit B, đồng thời đưa các thay đổi về Working directory.

`.git reset --hard B`: mất hết

**-Check out đến ID:**

![a](https://i.imgur.com/0xhi8Yy.png)
  

lúc này bạn sẽ đứng ở lần sửa đổi đó (HEAD trỏ đến đó), nếu muốn lấy trạng thái lần sửa đổi này thì tại đây tạo branch :

  ![enter image description here](https://i.imgur.com/AiSk2Cb.png)

**-Merge:**

  ![enter image description here](https://i.imgur.com/hZfrwEx.png)

  ![enter image description here](https://i.imgur.com/LDASp5S.png)

  - Result: 

![enter image description here](https://i.imgur.com/xMHZmwD.png)
 
**-Merge and rebase:**

![a](https://i.imgur.com/ERhIeUs.png)

+pull and push:

![enter image description here](https://i.imgur.com/aDDfR2q.png)
  
+**interactive rebase**: chỉnh sửa các commit 

  ![enter image description here](https://i.imgur.com/mley9GN.png)

. cung cấp nhiều option để lữa chon ( ví dụ như e đê chỉnh commit)

+**git cherry-pick**: apply 1 commit cho cả 2 branch
 `git checkout B` ; 
 `git cherry-pick A `#commit cuối của A merge vào B

_**6.2 Các thao tác với git remote**_

**-Add remote:**

![enter image description here](https://i.imgur.com/EgkEudC.png)
  
**-rename remote:**

![enter image description here](https://i.imgur.com/yyHdzkv.png)

  
**-update remote:**

`git remote set-url minh https://github.com/_USERNAME_/_REPOSITORY_git`

**-Prune remote branch:**
`git branch -a`: # show branch in local and remote

`git remote prune origin --dry-run`  #lists branches that can be deleted/pruned on your local

`git remote prune origin`

+del local branch: `git branch -d namebranch`

_**6.3 Các thao tác với Stash**_

>Git stash được sử dụng khi muốn lưu lại các thay đổi chưa commit, thường rất hữu dụng khi bạn muốn đổi sang 1 branch khác mà lại đang làm dở ở branch hiện tại.

-Lưu toàn bộ nội dung công việc đang làm dở

`git stash save` _# or just "git stash"_

 
-Sau khi đã git stash 1 hoặc vài lần, bạn có thể xem lại danh sách các lần lưu thay đổi bằng câu lệnh $ git stash list

![enter image description here](https://i.imgur.com/ZORrCbh.png)

- Lấy lại thay đổi từ lần thứ n:

![enter image description here](https://i.imgur.com/gV49lb0.png)
  
- clear hết stash:

![enter image description here](https://i.imgur.com/j534a5c.png)

**Tìm** **hiểu cách dùng gitlab**

**1.Tạo project, clone về laptop, push file**

**2.Repository**

-**Compare**: so sánh 2 obj

![enter image description here](https://i.imgur.com/o3Gy72c.png
 ex. so sánh 2 commit

-**Graph**: xem gitflow

![enter image description here](https://i.imgur.com/SlRACfE.png)

**2.Issues**

**-List:** On the Issues List, you can view all issues in the current project

![enter image description here](https://i.imgur.com/ojfNlFn.png)

-**Label**: gán nhãn cho issues để phân loại, có thể dựa vào đó để tìm kiếm

  
-**Milestones:** Milestones in GitLab are a way to track issues and merge requests created to achieve a broader goal in a certain period of time

![enter image description here](https://i.imgur.com/WqCnvk3.png)
  
**-Board:** is a software project management tool used to plan, organize, and visualize a workflow for a feature or product release.

![enter image description here](https://i.imgur.com/h9h5YAC.png)

**3. Merge request**

![enter image description here](https://i.imgur.com/cb0NoW8.png)

→ tương đương với việc từ branches master $git merge mrHac

  ![enter image description here](https://i.imgur.com/XfRlZLl.png)

**+ Tính năng:**

![enter image description here](https://i.imgur.com/WdlNjll.png)

![enter image description here](https://i.imgur.com/OtkYfDd.png)

**kết quả:**

![enter image description here](https://i.imgur.com/NfJznLD.png)
  
**4.Time tracking**

Dùng để báo cáo tiến độ or hạn, khai báo trong **[Discussion](https://gitlab.com/genius98nd/gitlab/issues/5#discussion-tab)**

`/estimate time `: thời gian ước tính

`/spend time` : time đã hoàn thành

- Due date : Hạn

- Assignee: chỉ định người nhận task

**5.Add ssh key**

>Là một cặp key được dùng để mã hóa bất đối xứng, gồm có public key và private key. SSH key dùng để xác minh quyền truy cập và mã hóa nội dung để tránh tấn công MITM

![enter image description here](https://i.imgur.com/icNANzh.png)

Cách tạo ssh key:

![enter image description here](https://i.imgur.com/9tKTZeU.png)
  
ssh key public trên sẽ được lưu ở /home/genis/.ssh/id_ras.pub . copy rồi add vào gitlab.

-Kết quả dùng ssh để clone :

![enter image description here](https://i.imgur.com/gafwtEy.png)
  

**Tìm hiểu về GitLab workflow**

**5.Protected Branches**

**-** Mặc định, protected branches sẽ:
+ Chặn việc tạo ra nó nếu chưa được tạo, ngoại trừ user với quyền Maintainer

+ Chặn pushes nếu user không có quyền Allowed

+ Chặn anyone dùng force push or del vào branch

**Note:** Gitlab admin cho phép push vào protected branches
 -Config protected branches

+ Default master branch is protected

+ Setting → repository →protect branch

![enter image description here](https://i.imgur.com/QLimnRa.png)

![enter image description here](https://i.imgur.com/r28Drr2.png)

Trong đó:

+ Branch: tên branch or wildcard( mutil brand theo tiền tố)

+ Alowed to merge, push : có thể chọn developer, maintainer , user

+ Require approval : cần ủy quyền của chủ sở hữu khi push or merge trước khi action đó đc thực thi

  

**6.Protected tag**

**-** Cho phép kiểm soát user có quyền tạo tags và chặn việc update or del ngẫu nhiên sau khi được tạo. Mỗi rule cho phép bạn match 1 tên thẻ riêng or dùng wildcard để control multi tags cùng lúc.

-Default user ko có quyền Maintainer sẽ bị chặn tạo thẻ

-Config protected tags (cần có quyền maintainer)

-Setting → Repository:

![enter image description here](https://i.imgur.com/DImgGhc.png)

**7. Tham khảo git branching model**

### a)Model with milestones

![enter image description here](https://i.imgur.com/iJtPQ0K.png)

-Giả sử dev cycle tên là “foo”:

+ Giai đoạn đầu dev trên master branch, khi push đến tag x.0.0b3 ngừng phát triển tính năng

+ Tập trung vào fix bug, nếu ổn định rồi thì kết thúc giai đoạn đóng băng này

+ Push a x.0.0.0rc1 tag và publuc project trên nhánh “pre-release stable/foo”, phát triển tính năng tiếp trên nhánh master

+ Nếu gặp bugs lớn trên bản public, quay lại fix bug, fix xong thì tag cho nó cái thẻ RC

+ Đến ngày phát hành, gắn thẻ x.0.0 vào RC hiện tại. Điểm stable/foo được quản lý bởi Stable team,nếu nhận các lỗi nghiêm trọng thì sẽ quay lại trình tự trên đến điểm x0.1 (điểm phát hành kế tiếp) đến hết vòng đời

b) **Model with intermediary releases**

![enter image description here](https://i.imgur.com/4CPNSdT.png)

Ở model này các bản phát hình được thực hiện theo ý muốn trong chu kì phát triển “foo”, tăng số phiên bản dựa trên sự thay đổi của bản phát hình trước. Khi kết thúc chu kì phát triền, nhóm sẽ đề xuất 1 phiên bản phát hành ‘foo; cuối cùng. Tại đây tạo ra nhánh mới (“stable/foo”), quản lý bởi Stable Team, sẽ được sửa các lỗi nghiêm trọng và các bản phát hành ổn định trong tương lai sẽ được gắn thẻ trên nhánh đó