**I. Overview về docker container**

  

**1. Container và VM**

**a)VM**

![alt txt](https://i.imgur.com/ISB3TKA.png)


-Vms về bản chất là một giả lập của một máy tính để thực thi các ứng dụng giống như một máy tính thật, cung cấp sự ảo hóa về phần cứng. Host machine sẽ cung cấp cho VMs những tài nguyên như là RAM, CPU. Những tài nguyên đó sẽ được phân bổ giữa các VMs theo cách mà bạn cho là hợp lý.

-Những VMs chạy trên host machine thường được gọi là guest machine. Guest machine này sẽ chứa tất cả những thứ mà hệ thống cần để chạy ứng dụng như hệ điều hành (OS), system binaries và libraries. VMs chạy trên hệ điều hành của host machine và không thể truy cập trực tiếp đến phần cứng mà phải thông qua hệ điều hành.

 
**Nhược:**

 
-Về tài nguyên: Khi bạn chạy máy ảo, bạn phải cung cấp "cứng" dung lượng ổ cứng cũng như ram cho máy ảo đó, bật máy ảo lên để đó không làm gì thì máy thật cũng phải phân phát tài nguyên.

-Về thời gian: Việc khởi động, shutdown khá lâu, có thể lên tới hàng phút.t


**b) Container**

![sd](https://i.imgur.com/gkq9tHa.png)

-Container không giống như VMs, Container không cung cấp sự ảo hóa về phần cứng. Một Container cung cấp ảo hóa ở cấp hệ điều hành bằng một khái niệm trừu tượng là “user space”. Sự khác nhau lớn nhất của Container và VMs là Container có thể chia sẻ host system’s kernel với các container khác

-Ưu điểm:

-   Linh động: Triển khai ở bất kỳ nơi đâu do sự phụ thuộc của ứng dụng vào tầng OS cũng như cơ sở hạ tầng được loại bỏ.
    
-   Nhanh: Do chia sẻ host OS nên container có thể được tạo gần như một cách tức thì.
    
-   Nhẹ: Container cũng sử dụng chung các images nên cũng không tốn nhiều disks.
    
-   Đồng nhất :Khi nhiều người cùng phát triển trong cùng một dự án sẽ không bị sự sai khác về mặt môi trường.
    
-   Đóng gói: Có thể ẩn môi trường bao gồm cả app vào trong một gói được gọi là container. Có thể test được các container. Việc bỏ hay tạo lại container rất dễ dàng.
    

**2. Docker là gì ?**

Docker là một nền tảng cho developers và sysadmin để develop, deploy và run application với container. Nó cho phép tạo các môi trường độc lập và tách biệt để khởi chạy và phát triển ứng dụng và môi trường này được gọi là container. Khi cần deploy lên bất kỳ server nào chỉ cần run container của Docker thì application của bạn sẽ được khởi chạy ngay lập tức.

-Docker runs in a client-server architecture that means docker client can connect to the docker host locally or remotely.

***Docker objects:**

-   **Images:** là 1 template chỉ đọc để tạo ra Docker container. Thường thì nó sẽ dựa trên 1 images khác, và thêm vào 1 số customization. Ví dụ như bạn có thể xây dựng 1 image dựa trên image ubuntu, sau đó install web server (như Apache) và application của bạn + 1 số thứ để app đó có thể chạy.
    
    -Bạn có thể tự tạo image hoặc sử dụng image của người khác đã push lên registry or docker hub. Để tự tạo image, bạn viết 1 file tên là “Dockerfile” rồi build nó. Mỗi instruction trong Dockerfile là 1 lớp trong image, vì thế khi bạn thay đổi docker file thì phần nào thanh đổi thì lớp image tương ứng mới bị rebuid lại
    
-   **Container: ....**
    
-   **Service:** cho phép scale container qua nhiều Docker daemon, tất cả hoạt động cùng nhau như 1 swarm với multi managers và workers.
    

**3. Cài đặt docker**

`$sudo apt-get update`

`$sudo apt install docker.io`

`$sudo systemctl start docker`

`$sudo systemctl enable docker`

-Sử dụng Docker Command mà không cần quyền sudo:

`$ sudo addgroup --system docker`

`$ sudo adduser <username> docker`

`$ newgrp docker`

**`4. Cấu hình proxy`**

**`a) Cho docker daemon`**

![sde](https://i.imgur.com/6Gp6bzv.png)

-**Docker daemon** :Docker daemon (dockerd) nghe các yêu cầu từ Docker API và quản lý các đối tượng Docker như =images, containers, network và volumn. Một daemon cũng có thể giao tiếp với các daemon khác để quản lý các Docker services.

Ví dụ:

-   Terminal gửi đoạn lệnh vào Docker CLI
    
-   Docker CLI sau khi nhận đoạn lệnh trên sẽ gửi nó đến Docker Server (hay còn gọi là Docker Daemon). Chúng ta không bao giờ tương tác trực tiếp với Docker Daemon; mọi thao tác đều sẽ phải thông qua Docker CLI
    
-   Docker Daemon, sau khi nhận yêu cầu từ Docker CLI, biết rằng chúng ta đang muốn khởi tạo một Docker Container từ Docker Image của PostgreSQL. Việc đầu tiên Docker Daemon làm đó là kiểm tra xem trong Image Cache đã tồn tại file Docker Image của PostgreSQL chưa. Trong trường hợp này, vì đây là lần đầu tiên chúng ta chạy câu lệnh Docker do đó trong Image Cache sẽ không có gì cả, khi đó Docker Daemon sẽ gửi lên Docker Hub một request yêu cầu download một Docker Image của PostgreSQL.
    
-   **Docker Image** là một file chứa tất cả các dependencies và cấu hình cần thiết để chạy một program.
    
-   Còn **Docker Hub** là một nơi lưu trữ tất cả các Docker Image mà bạn có thể download miễn phí
    
-   Sau khi Docker Image của PostgreSQL được lưu vào ổ cứng, Docker Daemon sẽ sử dụng Docker Image này để tạo ra một **Docker Container** chạy PostgreSQL. **Docker Container** này bản chất là một program được máy tính cấp cho một vùng nhớ riêng, một vùng ổ cứng riêng

    ![sd](https://i.imgur.com/weiHytE.png)
    

→mỗi program được khởi tạo từ Docker Image được gọi là Docker Container.

Docker Container bản chất là một process (hoặc một nhóm các process) được kernel cấp phát cho một lượng resource nhất định để sử dụng. Mỗi khi Docker Container thực hiện 1 system call đến kernel, kernel sẽ điều hướng nó đến một phân vùng nhất định trên ổ cứng với các tài nguyên (RAM, CPU, network bandwidth) tương ứng.

**- Config proxy cho docker daemon**

**+** Tạo thư mục vào file, với nội dung như ảnh dưới, thay thế “myproxy, hostname, port “ tương ứng

  ![sde](https://i.imgur.com/qDHvyAl.png)

  ![a](https://i.imgur.com/mxlix6x.png)
  
+Reload và restart lại  

![a](https://i.imgur.com/jgPpRTq.png)

**b) Cho Docker client**

-Docker Client: là một công cụ giúp người dùng giao tiếp với Docker host.

(The Docker client (docker) is the primary way that many Docker users interact with Docker. When you use commands such as docker run, the client sends these commands to dockerd, which carries them out. The docker command uses the Docker API. The Docker client can communicate with more than one daemon.)

-If you’re building Docker images with docker build or docker-compose, you will want to configure proxy settings for the build process. Despite the fact that Docker daemon has the correct proxy settings, during the build process commands that update the image being built (i.e. apk update, apt-get update etc.) will fail without this step.
- tạo và sửa file ~/.docker/config.json :

![a](https://i.imgur.com/9mF5Uzk.png)
  

**6. Hello world với Do**

![a](https://i.imgur.com/z62oQ9T.png)
![a](https://i.imgur.com/VTgW20D.png)
![ae](https://i.imgur.com/J93sssY.png)

**II.Các câu lệnh cơ bản**

**1.Build and run,tag**

ref : [https://docs.docker.com/get-started/part2/](https://docs.docker.com/get-started/part2/)

*** Tạo image:**

- Lệnh Build được sử dụng khi bạn muốn xây dựng 1 image, n hoạt động bằng cách viết 1 Dockerfile, chứa các nội dung cần thiết cho 1 app, rồi từ đó build lên image

![a](https://i.imgur.com/ascaWph.png)
  

- -t : Docker provides a way to tag your images with friendly names and optionally a tag of your
      choosing, in the 'name:tag' format,

- borabanei:1.0 : borabanei is name image and 1.0 is tag.

- ". ": chỉ đường dẫn của dockerfile, “.” để chỉ dockerfile đang ở local

 ![a](https://i.imgur.com/G43Qi7r.png)

  -Download image từ docker hub :

   `sudo docker run centos`
    
***Start container:**

**+** From image :

![a](https://i.imgur.com/5PVWUE8.png)
  

- --publish : chuyển traffic đến port 8000(trái) trên host và 8080(phải) trên container

- --detach: run container trên bachground

- --name: gán cho container 1 cái tên (trường hơp này là bb)

-  borabanei:1.0 : name_image:tag_image

Access vào container đang chạy: 127.0.0.1:8000

![a](https://i.imgur.com/RrKKvZ2.png)
  
* 1 số lệnh check và optina*

![a](https://i.imgur.com/x2nCWMP.png) 

- Xóa 1 image

  `docker rmi IDimage`
    
-Check container đang chạy:

`docker container ls`
    
`  docker ps`
    

thêm -a : list all container

- Xóa, pause/unpause container:

` docker container rm --force bb`
    
`  docker pause/unpause ContainerID`
    

--force : khi container đang chạy , ‘bb’ là tên ref đến id container

- stop services :

` docker-compose stop`
    

- xem các services đang chạy:

`  docker-compose ps`
    
**2. Push , pull**

**-push :** share image lên repo ( ex: docker hub)

- step 1: create account in docker hub , create repo , $docker login in terminal

![ae](https://i.imgur.com/lwXWB7h.png)
  
- step 2: chỉnh sửa image sao cho nó có dạng “name_dockerhub/nameimage” với name_docekerhub trùng với tài khoản dockerhub (trường hợp này là genius98nd) , cú pháp sửa tên image:

`$docker image tag borabanei:1.0 genius98nd/borabanei:1.0`
- step 3: push image lên dockerhub

`$docker push genius98nd/borabanei:1.0`

**-Pull:**

`$docker pull genius98nd/borabanei:1.0`

  
**III. Kiến thức thêm**

**1.Docker CE/Docker EE**
> ref: [https://boxboat.com/2018/12/07/docker-ce-vs-docker-ee/](https://boxboat.com/2018/12/07/docker-ce-vs-docker-ee/)

- Docker CE được phát hành năm 2013, là platform containerization mã nguồn mở miễn phí, có thể chạy trên nhiều hệ điều hành. Thường dành cho development . update mỗi tháng (với edge) và mỗi quý (với stable)

- Docker EE có thể nói là phiên bản trả phí premium của CE, released năm 2017 dành cho business-critical deployments, update mỗi quý

- Cả 2 đều được update định kì, tên phiên bản ( ex 17.03) theo định dạng năm.tháng update , và đều có sẵn trên những hdh phổ biến cũng như hạ tầng cloud

Docker EE cung cấp nhiều feature giúp doanh nghiệp lauch, quản lý và bảo mật:

- Gain access to certified Docker images and plugins

- View your container clusters in a single pane view

- Access controls for cluster and image management

- Run Docker EE engine with FIPS 140-2 certification

- Continuous vulnerability monitoring and Docker Security Scanning

  

**2.Docker Engine**

> Docker Engine là ứng dụng client-server, như một công cụ để đóng gói ứng dụng, hỗ trợ công nghệ container để xử lý các nhiệm vụ và quy trình công việc liên quan đến việc xây dựng các ứng dụng dựa trên vùng chứa (container). với các thành phần chính.

![a](https://i.imgur.com/XxxaCRU.png)
  
![a](https://i.imgur.com/JX7XZ8y.png)
  

  

**3. Docker Compose**

>ref: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)
là công cụ giúp định nghĩa và khởi chạy multi-container Docker applications

* Các features:

-   Multi isolated environments trên 1 host đơn
    
-   Toàn ven dữ liệu ở volume khi container được tạo (nó sẽ tìm bất bị container nào đc chạy từ trước, và copy vào container mới
    
-   Chỉ tạo lại container khi bị thay đổi (Khi restart 1 service mà ko bị thay đổi, compose sử dụng lại container hiện có.
    
-   Support variable trong Compose file
    

Example: [https://gitlab.com/genius98nd/baocaotrain/-/tree/docker](https://gitlab.com/genius98nd/baocaotrain/-/tree/docker)

kết quả khi chạy các modify:

![a](https://i.imgur.com/y11qs9f.png)

+ Khi thêm volume, n cho phép bạn thay đổi code ngay khi đang chạy mà không cần rebuild lại image

  ![a](https://i.imgur.com/qeWGcKs.png)

  


***Docker instruction:** [https://gitlab.com/genius98nd/baocaotrain/-/tree/appdockertrain](https://gitlab.com/genius98nd/baocaotrain/-/tree/appdockertrain)

**-FROM:** Là base image để chúng ta tiến hành build một image mới. Command này phải được đặt trên cùng của Dockerfile

**-LABLE:** gán nhãn

` LABEL com.example.version="0.0.1-beta" com.example.release-date="2015-02-12"`
    

**-RUN:** RUN thực thi (các) lệnh trong một layer mới Ví dụ: nó thường được sử dụng để cài đặt các gói phần mềm.

    RUN apt-get update && apt-get install -y \ 
    package-bar \
    
    package-baz \
    
    package-foo=1.3.*
    

**-CMD**: Sử dụng khi muốn thực thi (execute) một command trong quá trình build một container mới từ docker image, thường có cấu trúc : CMD ["executable", "param1", "param2"…]

` CMD ["apache2","-DFOREGROUND"]`

` CMD ["flask","run"]` Set the default command for the container to flask run

**-EXPOSE:** port của container nghe các kết nối

`  EXPOSE 80`
    

**-ENV:** Định nghĩa các biến môi trường

**-COPY, ADD:** cùng chức năng, copy được dùng nhiều hơn khi copy file cục bộ, khi nạp các gói từ xa ko nên dùng add mà nên thay bằng curl or wget

`   COPY requirements.txt /tmp/`
    

**-ENTRYPOINT**: Định nghĩa những command mặc định, cái mà sẽ được chạy khi container running.

-**WORKDIR**: Định nghĩa working directory cho container khi được tạo 

-**VOLUME**: Cho phép truy cập / liên kết thư mục giữa các container và máy chủ (host machine)

`  VOLUME [/myvol]`
  

**4. Docker volume and bind mounts **

Default tất cả những file tạo từ trong container sẽ được lưu trữ ở lớp container có thể ghi dữ liệu sẽ ko tồn tại nếu container đó khong tồn tại -> khó lấy ra nếu quá trình khác cần
  khó chi chuyển dữ liệu đi nơi khác 
 Có 2 option là dùng volume và bind mounts
- Volumes are stored in a part of the host filesystem which is managed by Docker (/var/lib/docker/volumes/ on Linux).
  Non-Docker processes should not modify this part of the filesystem. Volumes are the best way to persist data in Docker.
- Volumes also support the use of volume drivers, which allow you to store your data on remote hosts or cloud providers, among other possibilities.
- Sharing data among multiple running containers. If you don’t explicitly create it, a volume is created the first time it is mounted into a container. When that container stops or is removed, the volume still exists. Multiple containers can mount the same volume simultaneously, either read-write or read-only. Volumes are only removed when you explicitly remove them.
- When the Docker host is not guaranteed to have a given directory or file structure. Volumes help you decouple the configuration of the Docker host from the container runtime.
- When you want to store your container’s data on a remote host or a cloud provider, rather than locally.
- When you need to back up, restore, or migrate data from one Docker host to another, volumes are a better choice. 

- Bind mounts may be stored anywhere on the host system. They may even be important system files or directories. 
Non-Docker processes on the Docker host or a Docker container can modify them at any time.
- Sharing configuration files from the host machine to containers. This is how Docker provides DNS resolution to containers by default, by mounting /etc/resolv.conf from the host machine into each container
- Sharing source code or build artifacts between a development environment on the Docker host and a container. For instance, you may mount a Maven target/ directory into a container, and each time you build the Maven project on the Docker host, the container gets access to the rebuilt artifacts

![enter image description here](https://i.imgur.com/gsrVoTD.png)
    
+Sử dụng volume để gắn (mount) một thư mục nào đó trong host với container.

`Docker run -it -v /_var_/data ubuntu`

→ gắn thư mục /_var_/data vào container ubuntu

+Sử dụng volume để chia sẽ dữ liệu giữa các container:

.tạo container chứa volume

`docker create -v /linhlt --name volumecontainer ubuntu`

.Tạo container khác sử dụng container volumecontainer làm volume. Khi đó, mọi sự thay đổi trên container mới sẽ được cập nhật trong container volumecontainer:

`docker run -t -i --volumes-from volumecontainer ubuntu /bin/bash`

**5.Docker network**

`$docker network inspect bridge`  # xem chi tiết

**a) Use bridge networks**

-Mặc định khi chạy docker, default bridge network được tạo ra tự động. Bridge network được thể hiện bởi docker0 network. Trừ khi bạn chỉ định một option network khác bằng lệnh docker run --network=<NETWORK>, nếu ko Docker daemon sẽ tự động connect các containers tới loại network này, và các container chung mạng này sẽ giao tiếp được với nhau. Để giữa các container chạy trên các Docker daemon khác nhau, bạn cần quản lý định tuyển ở level OS or sử dụng overlay networks

- Bạn cũng có thể tự tạo ra user-defined networks, nó có nhiều điểm ưu việt hơn so với default:

-   Cung cấp cơ chế tự động DNS giữa 2 container, thay bằng phải sử dụng --link trong default
    
-   isolation tốt hơn vì ở mạng default nếu ko chỉ định --network thì container sẽ mặc định cho vào mạng này
    
-   Có thể gán và tách container ra khỏi vùng mạng nhanh chóng
    
-   Có thể config cho mỗi bridge riêng
    

-Tạo/xóa user-define bridge:

`docker network create/rm my-net`
    

-connect a container đến 1 user-define networks:

`docker create --name my-nginx --network my-net --publish 8080:80 nginx:latest`
    
`  docker network connect my-net my-nginx ` # khi container đang chạy
    

- disconnect :

` docker network disconnect my-net my-nginx`
    

-linhking ko được hỗ trợ trong đây, thay vào đó bạn có thể expose và pubish container ports trên các container trong networks này

![enter image description here](https://i.imgur.com/rWPqTwX.png)
  

**b) Overlay networks**

**-**Tạo ra 1 mạng distributed giữa các máy chủ docker , cho phép các container kết nối với nó và đảm bảo được mã hóa

- Khi bạn tạo 1 swarm ott join host vào swarm thì sẽ có 2 mạng được tạo trên host đó:

-   1 mạng overlay : ingress-default nếu ko connect đến user-define overlay networks
    
-   1 mạng birdge : docker_gwbirdge , cái kết nối docker daemon riêng đến các docker daemon khác trong 1 swarm
    

- tạo overlay networks sử dụng với swarm services:

-   docker network create -d overlay my-overlay
    

- tạo overlay để được sử dụng bởi swarm services or giao tiếp giữa các container trên các docker daemon khác nhau

` $ docker network create -d overlay --attachable my-attachable-overlay`

## Issue: Link and net host in docker   

### Link : Giúp contanier giao tiếp với nhau thông qua tên service or alisas 
 **Khi nào cần sử dụng --Link:**
  >Sử dụng trong mạng default bridge. Mặc định khi không chỉ định 1 network bằng --network flag và drive network, thì container sẽ join vào mạng default bridge-các container trong mạng này có thể giao tiếp với nhau, nhưng chỉ qua IP address, trừ khi thêm --link flag để có thể giao tiếp qua tên service or alisa, link này chỉ là link 1 chiều.

  **Example** 
  -  Run 2 container từ image không dùng --network flag, sẽ mặc định join vào default bridge net 
 
  ![enter image description here](https://i.imgur.com/ioNQjqe.png)

![enter image description here](https://i.imgur.com/cyZf4hR.png)

>Docker engine sẽ tự set thông tin link vào /etc/hosts của wp container:

![enter image description here](https://i.imgur.com/SW3P3Q3.png)
> Vì Link là 1 chiều nên /etc/hosts trên redis container sẽ không có ánh xạ IP của wp

![enter image description here](https://i.imgur.com/TJDIbft.png)

*Khi bọn e run docker-compose, thì nó sẽ tự tạo 1 mạng user-define, với tên định dạng: namedir_default*
> Trong mạng user-define, các container có thể giao tiếp với nhau qua tên service or alisa, lưu tại file /etc/hosts của container, nên vì thế mà không cần dùng link trong trường hợp này.

### Thử lại ví dụ này bằng việc chạy container ở net host


> Khi chạy ở mode net host, container’s network stack không bị cô lập khỏi Docker host  , không có IP riêng mà nó sẽ sử dụng chung namespace net host, Published ports sẽ bị discard (ports: 8080:80 will be ignored)

![enter image description here](https://i.imgur.com/MbJ1M9s.png)

* Trong case này, cả phpmyadmin và wordpress đều mặc định port listen 80 (ko bind port được ở mode net host) , vì thế nó sẽ bị conflict. Khi docker-compose up, wordpress không khởi động được

![enter image description here](https://i.imgur.com/xIZDZ4i.png)
 
 > giải pháp bọn em đưa ra:

-  Sửa dockerfile -> với expose port listen của 2 container khác nhau ->> build lại image  
   kết quả: chưa thành công thì source code bọn e ko tìm được thông tin về expose port để sửa
- Vào container phpmyadmin, sửa port listen 2 file /etc/apache2/ports.conf và /etc/apache2/sites-enable/000-default.conf . vì mặc định apache sẽ lấy thông tin port listen từ 2 file đấy 
- /etc/apache2/ports.conf
![enter image description here](https://i.imgur.com/ixDx6kE.png)
- /etc/apache2/sites-enable/000-default.conf
![enter image description here](https://i.imgur.com/fT4MKDb.png)
- `service apache2 restart`
- Note : Trước khi install vim để sửa file cần dùng lệnh "apt update", trường hợp fail có thể do chưa config proxy cho docker client .
- Config proxy cho docker client  : [https://docs.docker.com/network/proxy/](https://docs.docker.com/network/proxy/)

![enter image description here](https://i.imgur.com/z8ioTLk.png)
 - phpmyadmin sẽ chạy được ở port 90, còn wp chạy port 80 

 ### NAT network trong docker
> Default, khi tạo 1 container, nó sẽ không publish cổng nào của nó ra bên ngoài, trừ khi sử  dụng `-p` flag. Lúc này sẽ tạo ra 1 firewall rule, map port của container đến 1 port trên docker host

> Để check  các thông tin về  bind port, em sử dụng `iptables`, tất cả Docker rule về bind port sẽ nằm ở  `chain Docker`

![Screenshot_from_2020-03-20_06-28-28](/uploads/b2a98fd008894674d7e9e1242cda7960/Screenshot_from_2020-03-20_06-28-28.png)

*  `target : DNAT` (Destination Network Address Translation),ghi đè địa chỉ đích của gói tin.
*  `IN : !br-f20fbae` : áp dụng cho tất cả các kết nối đi vào trừ các kết nối trong mạng br-f20fbae
> các gói tin với  DesIP khác với IP trong vùng mạng br-f20fabe (vùng mạng của các container) sẽ được DNAT, cụ thể  nếu chúng có dạng IP:8080 sẽ được phân giải thành 172.20.0.2:80 - tương ứng với IP và expose port của phpmyadmin