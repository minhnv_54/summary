from ansible.module_utils.basic import AnsibleModule
import openpyxl
import os

def main():

    module = AnsibleModule(
        argument_spec = dict(
            fields = dict(type = 'str',required = True),
            facts = dict(type = 'str',required = True),
            path = dict(type = 'str',default = '/tmp/lam.xlsx'),
        ),
        supports_check_mode = True,
    )
    
    fields = list(str.split(module.params['fields']))
    facts = list(str.split(module.params['facts']))
    path = module.params['path']

    if os.path.exists(path):
        book = openpyxl.load_workbook(path)
        sheet = book.active
        sheet.append(facts)
        book.save(path)
    else:
        book = openpyxl.Workbook()   
        sheet = book.active
        sheet.append(fields)
        sheet.append(facts)

        book.save(path)

    module.exit_json(changed=True)

if __name__ == '__main__':
    main()
