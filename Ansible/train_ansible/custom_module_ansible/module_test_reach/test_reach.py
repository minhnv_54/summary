#!/usr/bin/python
from ansible.module_utils.basic import AnsibleModule
def test_reach(module, host, port, timeout):
    nc_path = module.get_bin_path('nc', required=True)
    args = [nc_path, "-z", "-w", str(timeout), host, str(port)]
    rc = module.run_command(args)
    return rc == 0

def main():
    module = AnsibleModule(argument_spec=dict(host=dict(reuired=True), port=dict(required=True, type='int'), timeout=dict(required=False, type='int',default=3)), supports_check_mode=True)
    
    if module.check_mode:
        module.exit_json(changed=False)
 
    host = module.params['host']
    port = module.params['port']
    timeout = module.params['timeout']
    
    if test_reach(module, host, port, timeout):
        module.exit_json(msg "Có thể đi tới %s:%s" % (host, port),changed=False)
    else: 
        msg = "Không thể đi tới %s:%s" % (host, port)
        module.fail_json(msg=msg) 

if __name__ == "__main__":
	main()