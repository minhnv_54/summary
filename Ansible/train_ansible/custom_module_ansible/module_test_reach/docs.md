
Describe: Kiểm tra khả năng reach từ các hosts trong inventory đến des_host:des_port

>Test 
Input: host=192.168.1.123 port=80 timeout=2 (đang chạy wordpress)

![](https://i.imgur.com/yCu2xpl.png)


![](https://i.imgur.com/XSt0vzx.png)

Input: host=192.168.1.123 port=81 timeout=5 (không chạy gì)

![](https://i.imgur.com/A7FEjq7.png)

![](https://i.imgur.com/6nirjvg.png)

## Phân tích code:
**Parsing Arguments**

 ``` 
  - name: "rechable"
    test_reach: host=192.168.1.123 port=80 timeout=2
 ```  
`host`, `port` và `timeout` là các tham số cần truyền, để tạo ra các tham số này , khởi tạo 1 đối tượng `AnsibleModule` bằng cách truyền cho nó 1 `argument_spec` - 1 dictionary với `key-value` tương ứng với `tenthamso-giatri`
```        
           module = AnsibleModule(
              argument_spec=dict(
                host=dict(reuired=True), 
                port=dict(required=True, type='int'), 
                timeout=dict(required=False, type='int',default=3)) 
```
`AnsibleMobulde` class được Ansible cung cấp cho chúng ta dễ dàng custom module trên Python:
*  Parsing the inputs
*  Return output in JSON format
* Invoke external programs 

Có 2 tham số required là host và port, còn timeout nếu ko khai báo sẽ default giá trị bằng 3.  
Một số option của args.
*  `required` : Nếu  True, argument is required
*  `default` : Giá trị default của args khi required=False
*  `choices` : List những value của arg 
*  `aliases` : Tên khác để gọi thay cho arg name 
*  `type` : Argument type(default: 'str').'str', 'list', 'dict', 'bool', 'int', 'float'

Khi khai báo thành công AnsibleModule obj `module = AnsibleModule(...)` , ta có thể access giá trị `agrs` thông qua `params` dictionary 
```
host = module.params["host"]
port = module.params["port"]
timeout = module.params["timeout"]
```  

**Check mode**
Ngoài argument_spec là tham số bắt buộc của AnsibleModule obj, còn có 1 vài option khác, như `supports_check_mode`, default: False, nếu để  `True` như trong code khi kết hơp với `-C` flag sẽ enable check mode - tương tự `dry run` sẽ báo cáo  những thay đổi chúng sẽ thực hiện thay nhưng ko làm thay đổi remote host. Luôn return success mà ko trả lỗi.

>For your module to support check mode, you must pass supports_check_mode=True when instantiating the AnsibleModule object. The AnsibleModule.check_mode attribute will evaluate to True when check mode is enabled.
```
module = AnsibleModule(
    argument_spec = dict(...),
    supports_check_mode=True
)

if module.check_mode:
    # Check if any changes would be made but don't actually make those changes
    module.exit_json(changed=False)
```
**Returning Success or Failure**  
*  `exit_json(changed=, msg=)` : sử dụng khi return success, nên update thêm state của changed 
*  `fail_json(msg=)` : sử dụng khi indicate failure

**Invoking External Commands**

AnsibleModule class cung cấp `run_command` method để gọi chương trình ngoài- cụ thể là module subprocess của python  
```
def test_reach(module, host, port, timeout):
    nc_path = module.get_bin_path('nc', required=True)
    args = [nc_path, "-z", "-w", str(timeout), host, str(port)]
    rc = module.run_command(args)
    return rc == 0
```
với args như trên là 1 list gồm các string, sau đó nó sẽ gọi subprocess.Popen xử lý với shell=False  
-các phần tử trong list của args gồm
*  `nc_path`: sử dụng `get_bin_path` để lấy ra đường dẫn hệ thống thực thi lệnh 'nc' tìm trong PATH của Ansible  , 'nc'- tương ứng với netcat cài đặt sẵn trong linux
* `-z`: option của netcat, để scanport, sẽ không gửi data nào mà chỉ xác định xem các cổng bên kia có mở hay ko 
* `-w`: option của netcat, đặt timeout 

rc =0 tương ứng với success (returncode) 