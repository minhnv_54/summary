Modelu excel giúp đọc or ghi vào file excel.   
Yêu cầu thư viện python: `openpyxl` ( `pip3 install openpyxl` )   
Khi ở chế độ ghi, nó nhận từng giá trị trong array `values` để ghi vào `cells` tương ứng, vì thế yêu cầu khai báo  số values = số  cells  và các giá trị cách nhau bởi khoảng trắng 
```
    elif action == "write":
        for index in range(len(cells)):
            sheet[cells[index]] = values[index]  # Ghi đè giá trị trong cell
        book.save(filename)
```

![](https://i.imgur.com/0mUBv5V.png)  

![](https://i.imgur.com/jybWHQy.png) 

Khi ở chế độ đọc, n tìm mỗi giá trị nằm trong cells tương ứng, append vào dict `results`, gán `result=results`, tại cuối task excel register kết quả vào biến `kq`. Debug var:`kq`  
```
    if action == "read":
        results = {}
        for cell in cells:
            results.update({cell:sheet[cell].value})
        book.save(filename)
        module.exit_json(changed=False, result=results)

```  
![](https://i.imgur.com/MMHhc3b.png)
