#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r''' 
---
module: excel 
description:
    - read/write file excel
requirement: openpyxl (pip3 install openpyxl) 
options:
    action:
        description:
        - read or write .
        type: str
        required: true
        choices: [ "read",  "write"]
    cells:
        description:
        - Các ô trên excell muốn thực hiện "action"
        required: true
    values:
         description:
         - Các giá trị để ghi vào các cell tương ứng
         required: true nếu action là write 
    filename:
         description:
         - Đường dẫn của file excel
         required: true 
'''

EXAMPLES = r'''
ex1:
---
 -  name: Test 
    hosts: localhost
   
    tasks:
      - name: read excel 
        excel:
          action: read
          cells: A1 B1
          filename: /home/genis/Desktop/test.xlsx
        register: kq
      - debug: 
         var: kq
ex2:
---
 -  name: Test 
    hosts: localhost
   
    tasks:
      - name: test write excel 
        excel:
          action: write
          cells: A1 B1
          values: 3 4
          filename: /home/genis/Desktop/test.xlsx
'''

from ansible.module_utils.basic import AnsibleModule
import openpyxl
import os

def rw_excel(module, book, action, cells, values, filename):
    sheet = book.active
    if action == "read":
        results = {}
        for cell in cells:
            results.update({cell:sheet[cell].value})
        book.save(filename)
        module.exit_json(changed=False, result=results)

    elif action == "write":
        for index in range(len(cells)):
            sheet[cells[index]] = values[index]  # Ghi de gia tri trong cell
        book.save(filename)
    module.exit_json(changed=True)

def main():
    module = AnsibleModule(
      argument_spec   = dict(
        action        = dict(required=True, choices=['write', 'read']),
        cells         = dict(required=True),
        values        = dict(),
        filename      = dict(required=True),
        ),
        supports_check_mode = True,
    )

    action     = module.params['action']
    cells      = list(str.split(module.params['cells']))
    if module.params['values'] != None:
        values     = list(str.split(module.params['values']))
    else:
        values = ""
    filename   = module.params['filename']

    if os.path.exists(filename):
        book  = openpyxl.load_workbook(filename)
        rw_excel(module,book, action, cells, values, filename)

    else:
        book  = openpyxl.Workbook()
        rw_excel(module,book, action, cells, values, filename)

if __name__ == '__main__':
    main()