
"Ansible scales both up and down" - không phải đề cập về số lượng mà chúng ta quản lý, mà là độ phức tạp của các job chúng ta đang cố gắng automate

- Ansible scale down well- bời những task đơn giản thì rất dễ ràng triển khai qua chúng
- Ansible scale up well- bởi chúng cung cấp nhiều cơ chế  phân tách các job phức tạp ra thành các phần nhỏ

Và trong Ansible, thì `role` là cơ chế chính để break 1 playbook thành các files. Nó giúp đơn giản hóa các playbook phức tạp, và chúng giúp dễ dàng trong việc tái sử dụng. Ví dụ như gán *database role* cho những hosts có chức năng là 1 database server.  
## I. Cấu trúc của role
Mỗi role trong Ansible đều có 1 tên, chẳng hạn như *database*, và các file liên kết với *database role* trong thư mục *roles/database*, nơi chứa các file và thư mục có cấu trúc như sau:
- **roles/database/task/main.yml**  
Chứa tasks của role, sẽ được thực thi khi role được chạy 
- **roles/database/files/**   
Chứa các file để deploy thông qua *database role*
- **roles/database/templates/**  
Chứa các jinja2 template files 
- **roles/database/handlers/main.yml**  
Chứa các handler, có thể được sử dụng bởi role này or bất kì role nào khác
- **roles/database/vars/main.yml**   
Chứa các biến của role, có thể override biến trong default/main.yml
- **roles/database/defaults/main.yml**   
Chứa các biến default của role, mà có thể bị ghi đè
- **roles/database/meta/main.yml**   
Chứa các metadata của role  
> 1 role không cần thiết phải có đủ cả 7 thư mục, nhưng khi sửa dụng, gần như mỗi thư mục phải chứa file `main.yml`, đây là file mặc định Ansible sẽ load khi chúng ta include 1 role

Có thể sử dụng Ansible Galaxy để tạo ra 1 role có cấu trúc như trên. Ansible Galaxy là 1 website để chứa các roles, nơi mọi ngươi có thể pull or push roles
- Cách tải 1 role: `$ansible-galaxy install -p ./roles user_upload.rolename  
![](https://i.imgur.com/eNJBFo7.png)
- Cách tạo cấu trúc 1 role: `ansible-galaxy init rolename`  
![](https://i.imgur.com/D7H0XBa.png)  

### II. Sử dụng Roles  
Có 3 cách: `roles`, `include_rules`, `import_rules`  
- `roles`: classic có trước Ansible 2.3 , 1 kiểu static, luôn được thức hiện đầu tiên ( từ khi `pre_task` được sử dung)
- `include_rules`: có từ Ansible 2.4, là kiểu `dynamic`, được xử lý trong thời gian chạy mà task đó gặp phải
- `import_rules`: có từ Ansible 2.3, là kiểu `static`, xử lý trong quá trình phân tính cú pháp playbook
```
---
 - hosts: dbserver
   roles:
     - database
```
  
**Flow : thứ tự thực hiện playbook khi include roles:**  
  - Thực hiện `pre_tasks` nếu có
  - Thực hiện `handlers` nếu được kích hoạt
  - Thực hiện các role trong `roles`, các role dependencies định nghĩa trong meta/main.yml sẽ được chạy trước
  - Thực hiện `task` trong play 
  - Thực hiện `handlers` nếu được kích hoạt
  - Thực hiện `post_tasks` nếu có
  - Thực hiện `handlers` nếu được kích hoạt

**Role Dependencies**  
Giả sử chúng ta có 2 role là *web* và *database*, cả 2 đêu cần NTP server được install trên máy chủ. Chúng ta có thể install NTP server trên cả 2 role trên, nhưng sẽ bị duplicate. Or tạo ra 1 role mới độc lập là `ntp` role ->> dễ quên ko add vào khi muốn include 2 role web or database.  
Ansible supports 1 feature là `dependent roles` trong trường hợp này. Khi bạn define 1 role, bạn có thể rằng nó phụ thuộc vào 1 hoặc nhiều role khác, và ansible sẽ đảm bảo những role được chỉ định này sẽ được thực thi trước

>Cho phép bạn tự động pull các roles khác khi đang thực hiện 1 role, được lưu tại meta/main.yml 

```
roles/database/meta/main.yml
---
 dependencies:
   - role: ntp
     var:
       ntp_server=ntp.ubuntu.com 
```  
**Template**  
Nơi chứa các jinja2 template file. Để gọi chúng trong task sử dụng module `template`:   
`template: src=ntp.conf.j2 dest=/etc/ntp.conf`  
lúc này nó sẽ tìm kiếm file ntp.conf.j2 trong thư mục template trong role để copy ra dest yêu cầu. Đuôi file ".j2" để chỉ ra file jinja2.  
Ví dụ về 1 template:  
![](https://i.imgur.com/UJbDAI9.png)  
Các biến trong template sẽ được lấy ra từ thư mục `var`, nếu không có tên biến tương ứng trong thư mục này thì nó sẽ tìm sang thư mục `default`  
![](https://i.imgur.com/u6WtWIG.png)  
, thay thế giá trị tương ứng đấy thay cho tên biến rồi copy file sau khi đã thay xong giá trị biến đến file đích.