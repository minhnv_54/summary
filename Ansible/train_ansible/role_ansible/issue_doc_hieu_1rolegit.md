@HaManhDong @daikk115 Phần em đang thực hiện được ạ 
> Task: đọc hiểu role,: link github https://github.com/openstack/kolla-ansible/tree/master/ansible/roles/aodh  
# Khi trong playbook import role aodh, n sẽ hoạt động qua các bước
> Step 1: run main.py trong /meta/main.py   

![](https://i.imgur.com/AcfuuAX.png)   
-> Thực hiện role common trước  
> Step 2: run main.py trong /tasks/main.py 

![](https://i.imgur.com/Q5sIFEu.png)  

-> Thực hiện các task trong file `{{ kolla_action }}.yml` cùng thư mục với file main.py này. Biến `kolla_action` sẽ được thay thế bằng giá trị được khai báo trong playbook.   
![](https://i.imgur.com/pLEVt8z.png) 
### Giả sử chạy file bootstraps.yml ###  
-Chạy lần lượt các task, đầu tiên là task `create aodh database`  
![](https://i.imgur.com/CrJ7dRD.png) 
- `become: true` : Cấp quyền `root` để thực hiện task này (nếu ko chỉ đụng thêm `become_user`)
- `kolla_toolbox`: gọi module kolla_toolbox, là module để gọi cái module ansible có trong kolla_toolbox container, được sử dụng bởi Kolla project. Nó yêu cầu 1 đối số (arg) là `module_name`- tên module ansible và các đối số optional khác ( như module_args, module_extra_vars, user)
- Task này sử dụng database mysql (`module_name: mysqldb`), với `module_args` là `login_host`,  `login_port`, `login_user`, `login_password`, các args này được truyền giá trị thông qua biến:  
 ![](https://i.imgur.com/4LFJbJp.png)  
giá trị biến được lấy qua /default (chứa `name`), etc/kolla/passwords.yml (có thể điền giá trị biến {{ database_password }} ở đây or khai báo ở playbook ) và /group_vars (chứa args còn lại)  
![](https://i.imgur.com/BOfGVK7.png) 
/ansible/group_vars/all.yml  
- `run_once: True` and  `delegate_to: "{{ groups['aodh-api'][0] }}"` : chỉ chạy task này 1 lần trên host đầu tiên trong group['aodh-api'] ( trong inventory ) và các host khác sẽ bỏ qua task này trong mỗi lần chạy playbook 
- `when: not use_preconfigured_databases | bool`: điều kiện để thực hiện task-Khi use_preconfigured_databases ==no  
 ![](https://i.imgur.com/C0vdaj6.png)  
Tương tự với task tiếp theo trong file bootraps.yml 
```
- name: Creating aodh database user and setting permissions
  become: true
  kolla_toolbox:
    module_name: mysql_user
    module_args:
      login_host: "{{ database_address }}"
      login_port: "{{ database_port }}"
      login_user: "{{ database_user }}"
      login_password: "{{ database_password }}"
      name: "{{ aodh_database_user }}"
      password: "{{ aodh_database_password }}"
      host: "%"
      priv: "{{ aodh_database_name }}.*:ALL"
      append_privs: "yes"
  run_once: True
  delegate_to: "{{ groups['aodh-api'][0] }}"
  when:
    - not use_preconfigured_databases | bool
``` 
Module mysql_user yêu cầu thêm 1 số arg  khác như:
- `host: "%"` - Em không biết @@ 
- `priv: " {{ aodh_database_name }}.*:ALL"` :chuỗi đặc quyền MYSQL với định dạng: db.table:priv1,priv2., nếu để db.*:ALL là có tất cả đặc quyền trên db 
- `append_privs: "yes"`: Append thêm quyền từ `priv` cho người dùng này thay vì ghi đè (default:no) 

`Include_task: bootstrap_service.yml` - thực hiện các task trong file bootstrap_service.yml 

### Bootstrap_service.yml ### 
```
 #/roles/aodh/tasks/bootstrap_service.yml
---
- name: Running aodh bootstrap container
  vars:
    aodh_api: "{{ aodh_services['aodh-api'] }}"
  become: true
  kolla_docker:
    action: "start_container"
    common_options: "{{ docker_common_options }}"
    detach: False
    environment:
      KOLLA_BOOTSTRAP:
      KOLLA_CONFIG_STRATEGY: "{{ config_strategy }}"
    image: "{{ aodh_api.image }}"
    labels:
      BOOTSTRAP:
    name: "bootstrap_aodh"
    restart_policy: no
    volumes: "{{ aodh_api.volumes|reject('equalto', '')|list }}"
  run_once: True
  delegate_to: "{{ groups[aodh_api.group][0] }}"
``` 
- `vars:` khai báo biến `aodh_api`  
 
![](https://i.imgur.com/JHqjp7x.png)  
aodh_api.group == aodh-api
- `become: true` leo thang đặc quyền lên *root*
-  `kolla_docker`: module để control Docker trong Kolla, với các args:
- - `action: "start_container"`: Hành động mà module sẽ thực hiện, require: True, gồm các lựa chọn (   compare_container, compare_image, create_volume, ensure_image, get_container_env, get_container_state, pull_image, remove_container, remove_image, remove_volume, recreate_or_restart_container, restart_container, start_container, stop_container, stop_container_and_remove_container) 
-  - `common_options: "{{ docker_common_options }}"` Là 1 dict chứa các thông số chung như thông tin đăng nhập (require: false) 
![](https://i.imgur.com/WadCYel.png) /ansible/group_vars 
- - `detach: False`: Không thoát ra khỏi container khi nó được tạo ra 
- - `environment`: setup môi trường cho container
- - `image: "{{ aodh_api.image }}"` docker image.
- - `restart_policy: no`  Không restart policy khi docker restart lại container 
- - `volumes: "{{ aodh_api.volumes|reject('equalto', '')|list }}"`: gồm những volume được khai báo ở aodh_api.volume, loại trừ những giá trị rỗng và danh sách các volume để dưới dạng list
- `run_once: True` and `delegate_to: "{{ groups[aodh_api.group][0] }}"` : chỉ thực hiện task ở host đầu tiên trong group( aodh-api) 
### Check-contaniers.yml ### 
```
 #/roles/aodh/tasks/check-containers.yml
---
- name: Check aodh containers
  become: true
  kolla_docker:
    action: "compare_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ item.value.container_name }}"
    image: "{{ item.value.image }}"
    volumes: "{{ item.value.volumes|reject('equalto', '')|list }}"
    dimensions: "{{ item.value.dimensions }}"
  when:
    - inventory_hostname in groups[item.value.group]
    - item.value.enabled | bool
  with_dict: "{{ aodh_services }}"
  notify:
    - "Restart {{ item.key }} container"
``` 
- Kiểm tra trạng thái của các container trên mỗi host trong mỗi group của `aodh_service`, nếu có thay đổi thì gọi handlers tương ứng để xử lý
- Module và args tương tự như các tệp trên, chỉ khác cách gọi biến-nó sử dụng dict để return cặp key/value của mỗi item trong dict-`with_dict: "{{ aodh_service }} "`  rồi gọi giá trị trong dict bằng `{{ item.value.key }}`. Để dễ hiểu hơn, sử dụng 1 ví dụ   

![Screenshot_from_2020-04-22_16-12-43](/uploads/a23cc9b42fcfd91d908ff416e297176b/Screenshot_from_2020-04-22_16-12-43.png)

![Screenshot_from_2020-04-22_16-13-16](/uploads/6dfea08677a6d7f866f77d9f790337d5/Screenshot_from_2020-04-22_16-13-16.png) 

- `notify: "Restart {{ item.key }} container"` nếu kết quả trả về trên host nào đó có change !=0 thì sẽ gọi handlers từ dir /handlers/main.yml . Đây là 1 file gồm nhiều task, nó sẽ chỉ thực hiện task có `name` khớp với notify. Giả sử với ví dụ trên tại image.key=aodh-api change!=0 notify kích hoạt handler với name: "restart aodh-api container" trùng với task có name tương ứng trong hander ( item.key gồm 1 list các key aodh-api, aodh-evaluator, aodh-listener, aodh-notifier) 
```
#/roles/aodh/handlers/main.yml
---
- name: Restart aodh-api container
  vars:
    service_name: "aodh-api"
    service: "{{ aodh_services[service_name] }}"
  become: true
  kolla_docker:
    action: "recreate_or_restart_container"
    common_options: "{{ docker_common_options }}"
    name: "{{ service.container_name }}"
    image: "{{ service.image }}"
    volumes: "{{ service.volumes|reject('equalto', '')|list }}"
    dimensions: "{{ service.dimensions }}"
  when:
    - kolla_action != "config"
``` 
Như `name` của `action` , handler này sẽ recreate or restart conainer thông qua module `kolla_docker`, với các thông số về `common_options`, `name`, `image`, `volumes`, và `dismensions` . Về mặt giá trị thì các thông số này đều tham chiếu vào cùng biến so với các thông số tương tự ở /tasks/check_container.yml , nó chỉ khác về cách gọi biến, trong file này sử dụng khai báo `var` để lấy các giá trị từ var đó. 
- Các handler chỉ chạy khi `kolla_action`-khái báo trong playbook để gọi file chạy != "config" - không phải đang thực hiện task trong file config.yml 

### clone.yml ###
```
roles/aodh/tasks/clone.yml 
---
- name: Cloning aodh source repository for development
  git:
    repo: "{{ aodh_git_repository }}"
    dest: "{{ kolla_dev_repos_directory }}/{{ project_name }}"
    update: "{{ aodh_dev_repos_pull }}"
    version: "{{ aodh_source_version }}"
```
- `repo: "{{ aodh_git_repository }}"` : (require) địa chỉ kho git
 ```
   # /roles/aodh/defaults/main.yml
     ####################
     aodh_git_repository: "{{ kolla_dev_repos_git }}/{{ project_name }}"
``` 
Quản lý git checkout
- `dest: "{{ kolla_dev_repos_directory }}/{{ project_name }}"` : đường dẫn kiểm tra, require trừ khi `clone: no`
- `update: "{{ aodh_dev_repos_pull }}"`: nếu `no`, không lấy bản sửa đổi mới từ repo gốc ( biến kia đang để giá trị `no`
- `version: "{{ aodh_source_version }}"` : version của repo checkout (ex: release-0.22)  
### config.yml ### 
Phân tích từng task.  

**Task1**
```
#ansible/roles/aodh/tasks/config.yml
---
- name: Ensuring config directories exist
  file:
    path: "{{ node_config_directory }}/{{ item.key }}"
    state: "directory"
    owner: "{{ config_owner_user }}"
    group: "{{ config_owner_group }}"
    mode: "0770"
  become: true
  when:
    - item.value.enabled | bool
    - inventory_hostname in groups[item.value.group]
  with_dict: "{{ aodh_services }}"
```
Đảm bảo rằng file config tồn tại (nếu chưa có thì tạo ), tùy chỉnh lại permission trong tất cả các host ở mỗi group của `aodh_services có enable:true`
 
Sử dụng module `file`- có thể thiết lập, remove các thuộc tính của file,symlink or thư mục
- `path: "{{ node_config_directory }}/{{ item.key }}"` : đường dẫn đến tệp được quản lý 
-  `state: "directory"` : tất cả các thư mục con trung gian trong `path` sẽ được tạo nếu chúng không tồn tại (state có 1 list choice: absent: directory, file, hard, link, touch, ref: [file](https://docs.ansible.com/ansible/latest/modules/file_module.html) ) 
- `owner: "{{ config_owner_user }}"`: tên người sở hữu tệp trên
- `group: "{{ config_owner_group }}"`: group sở hữu tệp trên 
- `mode: "0770"`: chỉ định quyền của user, group_user- other đối với tập tin, số 0 đầu thể hiện cho Ansible biết là số bát phân 
- `become: true`: Thực hiện ở chế độ root    

 Giá trị các biến trên được truy xuất từ ddaay:
```
#ansible/group_vars/all.yml
# The directory to store the config files on the destination node
node_config_directory: "/etc/kolla"

# The group which own node_config_directory, you can use a non-root
# user to deploy kolla
config_owner_user: "root"
config_owner_group: "root"
``` 

**Task2**  
``` 
#ansible/roles/aodh/tasks/config.yml

- name: Check if policies shall be overwritten
  stat:
    path: "{{ item }}"
  run_once: True
  delegate_to: localhost
  register: aodh_policy
  with_first_found:
    - files: "{{ supported_policy_format_list }}"
      paths:
        - "{{ node_custom_config }}/aodh/"
      skip: true
```
- `stat: path: "{{ item }}"` : module `stat` tương tự như trong linux, để lấy facts của file nằm trong đường dẫn `path` và chỉ được thực hiện trên localhost. Giá trị của biến `item` được lấy từ vòng lặp with_ `firts_found`-trả về đường dẫn đầy đủ đầu tiên được tìm thấy qua sự kết hợp giữa 1 list các `file` và `path` (`skip: true` để trả về kết qủa rỗng nếu không tìm thấy tập tin thay cho việc xuất hiện lỗi). Các thông tin về file lấy từ `stat` sẽ được lưu vào biến `aodh_policy` để sử dụng cho các task sau.   

**Task3** 
```  
- name: Set aodh policy file
  set_fact:
    aodh_policy_file: "{{ aodh_policy.results.0.stat.path | basename }}"
    aodh_policy_file_path: "{{ aodh_policy.results.0.stat.path }}"
  when:
    - aodh_policy.results
``` 
Tạo ra biến mới `aodh_policy_file` và `aodh_policy_file_path` có thể sử dụng trong file này qua module `set_fact` 

**Task5**  
``` 
- name: Copying over config.json files for services
  template:
    src: "{{ item.key }}.json.j2"
    dest: "{{ node_config_directory }}/{{ item.key }}/config.json"
    mode: "0660"
  become: true
  when:
    - item.value.enabled | bool
    - inventory_hostname in groups[item.value.group]
  with_dict: "{{ aodh_services }}"
  notify:
    - "Restart {{ item.key }} container"
```  
- Copy file từ "{{ item.key }}.json.j2" trong thư mục `templates` sau khi thay thế  giá trị của các biến đến `dest`- location trên các host ( `note_config_directory`= "/etc/kolla" - được khai báo trong /group_vars/all.yml), đồng thời gán quyền truy cập vào thư mục cho các user. 
- Hành động này chỉ được thực hiện trên các host trong mỗi `group` của `aodh_service` và `enable: true` trên `group` đó.
- Khi thực hiện thành công trên mỗi host ( lúc này `change` thay đổi ) , `notify` sẽ kích hoạt handlers trong /handlers/main.yml thực hiện task có name tương ứng.  
***Xử lý 1 file templates***  
Khi bắt đầu run task 5, trình sẽ gọi đến file aodh-api.json.j2 xử lý 
```
#roles/aodh/templates/aodh-api.json.j2

{% set aodh_cmd = 'apache2' if kolla_base_distro in ['ubuntu', 'debian'] else 'httpd' %}
{% set aodh_dir = 'apache2/conf-enabled' if kolla_base_distro in ['ubuntu', 'debian'] else 'httpd/conf.d' %}
{
    "command": "{{ aodh_cmd }} -DFOREGROUND",
    "config_files": [
        {
            "source": "{{ container_config_directory }}/aodh.conf",
            "dest": "/etc/aodh/aodh.conf",
            "owner": "aodh",
            "perm": "0600"
        },
        {
            "source": "{{ container_config_directory }}/wsgi-aodh.conf",
            "dest": "/etc/{{ aodh_dir }}/wsgi-aodh.conf",
            "owner": "root",
            "perm": "0600"
        }{% if aodh_policy_file is defined %},
        {
            "source": "{{ container_config_directory }}/{{ aodh_policy_file }}",
            "dest": "/etc/aodh/{{ aodh_policy_file }}",
            "owner": "aodh",
            "perm": "0600"
        }{% endif %}
    ],
    "permissions": [
       {
            "path": "/var/log/kolla/aodh",
            "owner": "aodh:kolla",
            "recurse": true
       }
  ]
}
```   
Nếu nhiều biến có cùng tên được xác định ở nhiều nơi khác nhau,
Ansible sẽ tìm giá trị thay thế cho biến qua các mức ưu tiên sau (tăng dần ): Ref: [Here](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html?fbclid=IwAR2ZdjDmPbFfOrsF8e2plBRZVN3oRSg4V2-MS5aa8zvXFVzokoOD7ChXAao#variable-precedence-where-should-i-put-a-variable)  
``` 
 
{% set aodh_cmd = 'apache2' if kolla_base_distro in ['ubuntu', 'debian'] else 'httpd' %}
{% set aodh_dir = 'apache2/conf-enabled' if kolla_base_distro in ['ubuntu', 'debian'] else 'httpd/conf.d' %}
```
- Kiểm tra giá trị của biến kolla_base_distro theo mức độ ưu tiên biến theo ref trên, để quyết định giá trị cho aodh_cmd và aodh_dir ( kolla_base_distro: "centos" vì thế aodh_cmd= "httpd" và aodh_dir="httpd/conf.d")
- Đoạn dưới thay thế  `container_config_directory` = "/var/lib/kolla/config_files" (/groups_vars/all.yml) và biến `aodh_policy_file` được lấy qua biến khai báo trong module `set_fact` 
** task6 ** 
```
- include_tasks: copy-certs.yml
  when:
    - kolla_copy_ca_into_containers | bool
``` 
Thực hiện các task trong file `copy-certs.yml` nếu `kolla_copy_ca_into_containers: yes` (hiện tại đang để `no`)  
```
#ansible/roles/aodh/tasks/copy-certs.yml
---
- name: "Copy certificates and keys for {{ project_name }}"
  import_role:
    role: service-cert-copy
  vars:
    project_services: "{{ aodh_services }}"
```  
** task7 **  
``` 
#ansible/roles/aodh/tasks/config.yml
- name: Copying over aodh.conf
  vars:
    service_name: "{{ item.key }}"
  merge_configs:
    sources:
      - "{{ role_path }}/templates/aodh.conf.j2"
      - "{{ node_custom_config }}/global.conf"
      - "{{ node_custom_config }}/aodh.conf"
      - "{{ node_custom_config }}/aodh/{{ item.key }}.conf"
      - "{{ node_custom_config }}/aodh/{{ inventory_hostname }}/aodh.conf"
    dest: "{{ node_config_directory }}/{{ item.key }}/aodh.conf"
    mode: "0660"
  become: true
  when:
    - item.value.enabled | bool
    - inventory_hostname in groups[item.value.group]
  with_dict: "{{ aodh_services }}"
  notify:
    - "Restart {{ item.key }} container" 
``` 
- Task này hợp nhất các file cấu hình kiểu `ini` thành 1 file duy nhất trên `dest` bằng cách sử dụng module `merge_conffig` với args `sources` là list các file cấu hình cần hợp nhất ( `node_config_directory`: "/etc/kolla") và `dest` là nơi lưu file
- Task này được thực hiện ở mode đặc quyền ( become: true), ở các host trong mỗi group của aodh_service. 
Hanlders sẽ xử lý theo name của notify nhưng với điều kiện `kolla_aciton !=config` ( có nghĩa là ở playbook khai báo kolla_action !=config, và trong file đó có `include_task: config.yml` thì lúc này hanlers sẽ chạy được )  
** task cuối **  
```
#ansible/roles/aodh/tasks/config.yml

- name: Copying over wsgi-aodh files for services
  vars:
    service: "{{ aodh_services['aodh-api'] }}"
  template:
    src: "wsgi-aodh.conf.j2"
    dest: "{{ node_config_directory }}/aodh-api/wsgi-aodh.conf"
    mode: "0660"
  become: true
  when:
    - inventory_hostname in groups[service.group]
    - service.enabled | bool
  notify:
    - "Restart aodh-api container"

- include_tasks: check-containers.yml
  when: kolla_action != "config"
```
  - Khi khai báo `vars: service: "{{ aodh_services['aodh-api'] }}"`  thì service sẽ có giá trị sau:
```
#roles/aodh/dedault/main.py  
aodh-api:
    container_name: aodh_api
    group: aodh-api
    enabled: true
    image: "{{ aodh_api_image_full }}"
    volumes: "{{ aodh_api_default_volumes + aodh_api_extra_volumes }}"
    dimensions: "{{ aodh_api_dimensions }}"
``` 
Các step sau như các task trước..