## I.Gather_facts
Khi chạy 1 playbook, trước khi thực hiện task đầu, chúng ta sẽ thấy thông tin này xuất hiện:
![](https://i.imgur.com/4tvQ6UG.png) 
- Ansible sẽ sử dụng module `setup` để thu thập các thông tin chi tiết về máy chủ: kiến ​​trúc CPU, hệ điều hành, địa chỉ IP, thông tin bộ nhớ, thông tin ổ đĩa, v.v. Thông tin này được lưu trữ trong các biến được gọi là `facts` và chúng hoạt động giống như bất kỳ biến nào khác 
> Ví dụ print tên hostname trong playbook  

![](https://i.imgur.com/hK1M8jW.png)
![](https://i.imgur.com/QpLvwub.png)
> List các fact trên 1 máy chủ: [Github](https://github.com/lorin/ansible-quickref/blob/master/facts.rst) 
### Viewing a Subset of Facts
>Ref: [setup](https://docs.ansible.com/ansible/latest/modules/setup_module.html)

Vì Ansible thu thập nhiều fact, module `setup` hỗ trợ tham số  `filter` cho phép bạn lọc theo fact name, or `gather_subset` (lọc theo all, min, hardware, network, virtual, ohai, and facter) ví dụ:
![](https://i.imgur.com/fBFp7Ws.png)

## II.Fact caching mode
**1. Gathering**

*Có 3 mode gathering:*
*  **Implicit** (default) : Cache plugin sẽ **bị bỏ qua**, Ansible sẽ thực hiện gathering facts trong mỗi lần run playbook
*  **Explicit** : Ngược lại với **Implicit**, Ansible sẽ không gathering facts (trừ khi thêm `gather_facts`: True trong playbook và chưa hết caching_time_out).
* Smart : Chỉ gathering facts những host chưa có hoặc hết thời gian timeout.
- Để tùy chỉnh chế độ này, chúng ta sẽ config trong file ansible.cfg
>Note:Thứ tự ưu tiên các config:  
ANSIBLE_CONFIG (environment variable if set)   
ansible.cfg (in the current directory)  
~/.ansible.cfg (in the home directory)   
/etc/ansible/ansible.cfg

 ![](https://i.imgur.com/BmAmatF.png)

* `gathering` : thiết lập mode gathering 
* `fact_caching`: chỉ định 1 bộ nhớ đệm fact ( thường là jsonfile or redis, `ansible-doc -t cache -l` để xem thêm 
* `fact_caching_connection` : Có thể hiểu là nơi lưu cache, đối với plugin jsonfile là lưu thành file với tên là tên của host tại đường dẫn /tmp/ansible_fact_cache
* `fact_caching_timeout` : (int) (defalut= 86400) Set thời gian timeout cho cache (Số giây), = 0  là không timeout.

>Note: 
Khi `gathering: smart`, Không được đặt `gather_facts: True or False` ở playbook  