# Iptables 


>Iptables do Netfilter Organiztion viết ra để tăng tính năng bảo mật trên hệ thống Linux. Là một hệ thống tường lửa (Firewall) tiêu chuẩn được cấu hình, tích hợp mặc định trong hầu hết các bản phân phối của hệ điều hành Linux (CentOS, Ubuntu…). Iptables hoạt động dựa trên việc phân loại và thực thi các package ra/vào theo các quy tắc được thiết lập từ trước.

Iptables bao gồm 2 phần là netfilter nằm bên trong nhân Linux và iptables nằm ở vùng ngoài nhân. iptables chịu trách nhiệm giao tiếp với người dùng và sau đó đẩy rules của người dùng vào cho netfilter xử lý. netfilter thực hiện công việc lọc các gói tin ở mức IP. netfilter làm việc trực tiếp ở trong nhân của Linux nhanh và không làm giảm tốc độ của hệ thống. 
![](https://imgur.com/JvXT4XT.png)

 Iptables ( command ) thao tác, kết nối trực tiếp với Netfilter thông qua 5 hook của netfilter


– Kiểm tra Iptables đã được cài đặt trong hệ thống:  `iptables --version`  
– Cấu trúc của iptables như sau: `iptables` -> `Tables` -> `Chains` -> `Rules`

![](https://imgur.com/BF4JDZo.png)
## Trong iptables được chia thành 3 bảng (table)
- Bảng mangle: bảng này để thay đổi QoS của gói tin như thay đôi các tham số TOS,TTL,..
- Bảng NAT: dùng để thay đổi địa chỉ đích,nguồn, port đích, nguồn của gói tin
- Bảng filter: áp dụng các chính sách lọc gói tin qua đó cho phép gói tin được qua hay không qua
![](https://imgur.com/mvTzVo5.png)

Chỉ có năm hook kernel của netfilter, do đó, chains từ nhiều bảng được đăng ký tại mỗi hook. Ví dụ, hai bảng có chains PREROUTING. Khi các chain này đăng ký tại hook NF_IP_PRE_ROUTING được liên kết, chúng chỉ định mức độ ưu tiên chỉ ra thứ tự mỗi chain PREROUTING của bảng được gọi. Mỗi quy tắc bên trong chuỗi PREROUTING ưu tiên cao nhất được đánh giá tuần tự trước khi chuyển sang chain PREROUTING tiếp theo.

![](https://imgur.com/wjxlACd.png)

![](https://i.imgur.com/3b4yUwo.png)
### 1. Bảng NAT
![](https://i.imgur.com/LnBFot5.png)

Trong bảng NAT có các chain và có 3 chain được xây dựng sẵn trong table NAT:
>Chain là một quy tắc xử lý các gói tin bao gồm nhiều rules có liên quan tới nhau.

- `Chain PREROUTING`: Các rule thuộc chain này sẽ được áp dụng ngay sau khi gói tin vừa đi vào đến dải mạng (Network Interface), dùng để thay đổi địa chỉ đích của gói tin và trong chain PREROUTING target(tác vụ) được sử dụng là DNAT (destination NAT)

![](https://i.imgur.com/X5SvVSI.png)  
Những gói tin tcp có port đích 8080 khi đi vào sẽ được DNAT thành 127.0.0.1:80 
- `Chain POSTROUTING`: Các rule thuộc chain này áp dụng cho các gói tin rời khỏi dải mạng (Network Interface) (để đi ra), đây là chain dùng để thay đổi địa chỉ nguồn của gói tin và targer được sư dụng là SNAT (source NAT)
![](https://i.imgur.com/cUt7zyA.png)
![](https://i.imgur.com/Lb1nG7q.png)
Câu lệnh này có ý nghĩa đổi địa chỉ nguồn đối với gói tin tcp thành 10.10.10.10 
> Remove rule trên: `sudo iptables -t nat -D POSTROUTING -p tcp -j SNAT --to-source 10.10.10.10`
- `Chain OUTPUT`: Rule trong chain này được thực thi ngay sau khi gói tin được tiến trình tạo ra. 